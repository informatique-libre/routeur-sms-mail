# Routeur SMS -> Mail

Du fait de la généralisation de la double authentification par SMS (DSP2 etc.) il s'avère de plus en plus compliqué de gérer des accès à des sites pour des groupes d'utilisateurs tels que des associations (le SMS est envoyé sur le numéro d'un des membres et c'est vraiment problématique en cas d'urgence ou de non disponibilité de la personne en question: voyage, congés, déplacement etc.).

Le but de ce projet est de permettre à un groupe d'utilisateurs de partager un numéro de téléphone pour recevoir collectivement des SMS par mail:
* l'association souscrit à un abonnement téléphone "le moins cher possible" (le but étant uniquement de recevoir des SMS), par exemple Free à 2€
* ce numéro de téléphone est associé à la structure
* la puce SIM est introduite dans le routeur SMS-Mail proposé ici
* la configuration du routeur est faite pour qu'il soit d'un côté connecté au WIFI, de l'autre à une alimentation électrique USB et enfin une adresse mail de destination pour faire suivre les SMS reçus
* l'astuce consiste à fournir une adresse mail qui répartie entre les membres de la structure (liste de diffusion ou liste d'alias mail, votre hébergeur mail saura forcément le faire)

Et c'est tout, vous laissez le routeur SMS/Mail allumé 24h/24 et lorsqu'un SMS arrive il le "transforme" en mail, tout le groupe est informé et peut éventuellement réagir en cas d'utilisation "étrange" ...

## Matériel nécessaire

Coup de chance il existe une carte tout en un qui regroupe tout ce qu'il faut ! la "TTGO t-call V1.3 ESP32 SIM800L" ... cherchez précisément ce modèle sur votre moteur de recherche et vous deviez pouvoir vous le procurer pour un prix qui varie de 10 à 30€ ...

Attention à bien prendre le modèle complet avec l'antenne ... si vous avez uniquement la carte il faudra ajouter une antenne pour pouvoir capter correctement le signal GSM.

## Logiciel

Tout est disponible sur le dépôt git : https://projets.cap-rel.fr/projects/routeur-sms-mail/repository

Et comme d'habitude, répliqué sur le git framagit pour plus de résilience : https://framagit.org/informatique-libre/routeur-sms-mail
