#define PINCODE "0000"                                  //Code PIN de la puce GSM
#define MAILADDR "nous@informatique-libre.com"          //Adresse mail de destination
#define MAILSENDER "Le Relais SMS d'Informatique-Libre" //Pour le from
#define MAILFROM ""                                     //Adresse de l'expéditeur
#define SMTPHOST ""                                     //Serveur smtp
#define SMTPPORT 587                                    //port du serveur smtp
#define SMTPLOGIN ""                                    //identifiant smtp
#define SMTPPASS ""                                     //mot de passe smtp
#define SMTPDOMAIN ""                                   //domaine du mail (du smtp)
