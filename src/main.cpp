/**
 * SMS -> Mail gateway
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "Adafruit_FONA.h"
#include <SPIFFS.h>
#include <WiFiSettings.h>
#include <ArduinoOTA.h>
#include <ESP_Mail_Client.h>
#include <WebServer.h>
#include <Update.h>
#include "config.h"

#define SIM800L_RX 27
#define SIM800L_TX 26
#define SIM800L_PWRKEY 4
#define SIM800L_RST 5
#define SIM800L_POWER 23

#define SMS_TARGET "0698744401"
#define SMS_TARGET2 ""
#define SMS_MESSAGE "SMS Gateway INLI Ready !"

char replybuffer[255];

//NTP
const char *ntpServer = "pool.ntp.org";
const long gmtOffset_sec = 3600;
const int daylightOffset_sec = 3600;

HardwareSerial *sim800lSerial = &Serial1;
Adafruit_FONA sim800l = Adafruit_FONA(SIM800L_PWRKEY);

uint8_t readline(char *buff, uint8_t maxbuff, uint16_t timeout = 0);

// #define LED_BLUE  13
// #define RELAY 14

String smsString = "";

// Define the SMTP Session object which used for SMTP transsport
SMTPSession smtp;

// Define the session config data which used to store the TCP session configuration
ESP_Mail_Session session;

//Web server for upgrades
WebServer server(80);

const char *www_username = "admin";
const char *www_password = "esp32";
// allows you to set the realm of authentication Default:"Login Required"
const char *www_realm = "Custom Auth Realm";
// the Content of the HTML response in case of Unautherized Access Default:empty
String authFailResponse = "Authentication Failed";

/*
 * Login page
 */
const char *loginIndex =
    "<form name='loginForm'>"
    "<table width='20%' bgcolor='A09F9F' align='center'>"
    "<tr>"
    "<td colspan=2>"
    "<center><font size=4><b>ESP32 Login Page</b></font></center>"
    "<br>"
    "</td>"
    "<br>"
    "<br>"
    "</tr>"
    "<td>Username:</td>"
    "<td><input type='text' size=25 name='userid'><br></td>"
    "</tr>"
    "<br>"
    "<br>"
    "<tr>"
    "<td>Password:</td>"
    "<td><input type='Password' size=25 name='pwd'><br></td>"
    "<br>"
    "<br>"
    "</tr>"
    "<tr>"
    "<td><input type='submit' onclick='check(this.form)' value='Login'></td>"
    "</tr>"
    "</table>"
    "</form>"
    "<script>"
    "function check(form)"
    "{"
    "if(form.userid.value=='admin' && form.pwd.value=='admin')"
    "{"
    "window.open('/serverIndex')"
    "}"
    "else"
    "{"
    " alert('Error Password or Username')/*displays error message*/"
    "}"
    "}"
    "</script>";

/*
 * Server Index Page
 */

const char *serverIndex =
    "<!DOCTYPE html>"
    "<html>"
    "<head>"
    "<script src='https://code.jquery.com/jquery-3.5.1.slim.min.js' integrity='sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=' crossorigin='anonymous'></script>"
    "</head>"
    "<body>"
    "<form method='POST' action='#' enctype='multipart/form-data' id='upload_form'>"
    "<input type='file' name='update'>"
    "<input type='submit' value='Update'>"
    "</form>"
    "<div id='prg'>progress: 0%</div>"
    "<script>"
    "$('form').submit(function(e){"
    "e.preventDefault();"
    "var form = $('#upload_form')[0];"
    "var data = new FormData(form);"
    " $.ajax({"
    "url: '/update',"
    "type: 'POST',"
    "data: data,"
    "contentType: false,"
    "processData:false,"
    "xhr: function() {"
    "var xhr = new window.XMLHttpRequest();"
    "xhr.upload.addEventListener('progress', function(evt) {"
    "if (evt.lengthComputable) {"
    "var per = evt.loaded / evt.total;"
    "$('#prg').html('progress: ' + Math.round(per*100) + '%');"
    "}"
    "}, false);"
    "return xhr;"
    "},"
    "success:function(d, s) {"
    "console.log('success!')"
    "},"
    "error: function (a, b, c) {"
    "}"
    "});"
    "});"
    "</script>"
    "</body>"
    "</html>";

void setup_ota()
{

  /*return index page which is stored in serverIndex */
  server.on("/", []() {
    if (!server.authenticate(www_username, www_password))
    //Basic Auth Method with Custom realm and Failure Response
    {
      return server.requestAuthentication(BASIC_AUTH, www_realm, authFailResponse);
    }
    else
    {
      server.sendHeader("Connection", "close");
      server.send(200, "text/html", serverIndex);
    }
  });
  /*handling uploading firmware file */
  server.on(
      "/update", HTTP_POST, []() {
    server.sendHeader("Connection", "close");
    server.send(200, "text/plain", (Update.hasError()) ? "FAIL" : "OK");
    ESP.restart(); }, []() {
    HTTPUpload& upload = server.upload();
    if (upload.status == UPLOAD_FILE_START) {
      Serial.printf("Update: %s\n", upload.filename.c_str());
      if (!Update.begin(UPDATE_SIZE_UNKNOWN)) { //start with max available size
        Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_WRITE) {
      /* flashing firmware to ESP*/
      if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
        Update.printError(Serial);
      }
    } else if (upload.status == UPLOAD_FILE_END) {
      if (Update.end(true)) { //true to set the size to the current progress
        Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
      } else {
        Update.printError(Serial);
      }
    } });
  server.begin();

  Serial.println(F("OTA is ready"));
  Serial.print(F("IP address: "));
  Serial.println(WiFi.localIP());

  Serial.print(F("Password: "));
  Serial.println(WiFiSettings.password);
}

void readSim800()
{
  delay(500);
  while (Serial.available()) // IDE serial l serial  sim800L
  {
    sim800l.write(Serial.read()); //Forward what Serial received to Software Serial Port
  }
  while (sim800l.available()) //serialsim800L l serial dial IDE
  {
    Serial.write(sim800l.read()); //Forward what Software Serial received to Serial Port
  }
}

void send_mail(String sbj, String msg)
{
  SMTP_Message message;
  String s = "[sms] ";
  s.concat(sbj);

  String dt = MailClient.Time.getDateTimeString();

  Serial.print(F("Date : "));
  Serial.println(dt);

  // Set the message headers
  message.sender.name = MAILSENDER;
  message.sender.email = MAILFROM;
  message.subject = s.c_str();
  message.date = dt.c_str();
  message.addRecipient("Groupe", MAILADDR);

  // Set the message content
  msg.concat(F("\n\n--\nLe Relais SMS d'Informatique-Libre\nLogiciel Libre / GNU GPLv3\nAdresse du projet: https://projets.cap-rel.fr/projects/routeur-sms-mail/wiki"));

  Serial.println(F("Message dans send_mail : "));
  Serial.println(msg);

  message.text.content = msg.c_str();
  // message.addHeader("Message-ID: <sms@i200.fr>");

  //Obligé de passer par la !
  String ladate = "Date: " + dt;
  message.addHeader(ladate.c_str());

  // Connect to server with the session config
  smtp.debug(0);
  smtp.connect(&session);

  // Start sending Email and close the session
  if (!MailClient.sendMail(&smtp, &message))
  {
    Serial.println("Error sending Email, " + smtp.errorReason());
  }
}

void setup()
{
  // pinMode(LED_BLUE, OUTPUT);
  // pinMode(RELAY, OUTPUT);
  pinMode(SIM800L_POWER, OUTPUT);

  // digitalWrite(LED_BLUE, HIGH);
  digitalWrite(SIM800L_POWER, HIGH);

  Serial.begin(115200);
  Serial.println(F("ESP32 with GSM SIM800L"));
  Serial.println(F("Initializing....(May take more than 10 seconds)"));

  // ======================= wifi
  SPIFFS.begin(true); // On first run, will format after failing to mount
  // Use stored credentials to connect to your WiFi access point.
  // If no credentials are stored or if the access point is out of reach,
  // an access point will be started with a captive portal to configure WiFi.
  WiFiSettings.hostname = "RouteurSMS-INLI";
  WiFiSettings.connect();
  WiFiSettings.secure = false;

  // Set callbacks to start OTA when the portal is active
  WiFiSettings.onPortal = []() {
    setup_ota();
  };
  WiFiSettings.onPortalWaitLoop = []() {
    ArduinoOTA.handle();
  };

  setup_ota(); // If you also want the OTA during regular execution

  // ==================== end wifi
  Serial.print(F("Wait 10 sec for SIM warmup "));
  for (int w = 0; w < 10; w++)
  {
    Serial.print(".");
    delay(1000);
  }
  Serial.println("");

  // Make it slow so its easy to read!
  sim800lSerial->begin(1200, SERIAL_8N1, SIM800L_TX, SIM800L_RX);
  if (!sim800l.begin(*sim800lSerial))
  {
    Serial.println(F("Couldn't find GSM SIM800L"));
    while (1)
      ;
  }
  Serial.println(F("GSM SIM800L is OK"));

  if (!sim800l.unlockSIM(PINCODE))
  {
    Serial.println(F("PIN CODE Failed"));
  }
  else
  {
    Serial.println(F("PIN CODE OK!"));
  }

  char imei[16] = {0}; // MUST use a 16 character buffer for IMEI!
  uint8_t imeiLen = sim800l.getIMEI(imei);
  if (imeiLen > 0)
  {
    Serial.print(F("SIM card IMEI: "));
    Serial.println(imei);
  }

  // Set up the FONA to send a +CMTI notification
  // when an SMS is received
  sim800lSerial->print("AT+CNMI=2,1\r\n");
  readSim800();
  Serial.print(F("SIM:"));
  Serial.println(sim800l.available());
  Serial.print(F("Reseau:"));
  Serial.println(sim800l.getNetworkStatus());

  //NTP
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);

  Serial.print(F("Recherche d'une connexion opérateur GSM "));

  //  * 1: Registered, home network
  //  * 5: Registered, roaming
  while (sim800l.getNetworkStatus() != 1 && sim800l.getNetworkStatus() != 5)
  {
    Serial.print(".");
    if (sim800l.getNetworkStatus() == 3)
    {
      //on fait quoi dans ce cas ?
      Serial.print(F("[denied]"));
    }
    delay(1000);
  }
  Serial.println(F(" [ok]"));
  Serial.print(F(" Etat de la connexion au reseau: "));
  Serial.println(sim800l.getNetworkStatus());

  Serial.println(F("GSM SIM800L Ready ..."));
  sim800l.print("AT\r\n");
  readSim800();
  sim800l.print("AT+CMGF=1\r\n");
  readSim800();

  Serial.print(F("Clean up SIM800L memory ..."));
  sim800l.print("AT+CMGDA=\"DEL ALL\"\r\n");
  readSim800();

  sim800l.print("AT+CMGD=1,4\r\n");
  readSim800();

  Serial.println(F("[done]"));

  //Envoie un SMS lors du reboot
  if (sim800l.sendSMS(SMS_TARGET, SMS_MESSAGE))
  {
    Serial.println("SMS ready ok");
  }
  else
  {
    Serial.println("SMS failed to send");
  }

  // Set the session config
  session.server.host_name = SMTPHOST;
  session.server.port = SMTPPORT;
  session.login.email = SMTPLOGIN;
  session.login.password = SMTPPASS;
  session.login.user_domain = SMTPDOMAIN;

  String mail("Bonjour,\n");
  mail.concat("Bonne nouvelle, votre relais SMS/Mail est lancé: ceci est le tout premier mail envoyé après son démarrage.\n");
  mail.concat("Vous devriez pouvoir vous connecter sur son interface à l'adresse suivante: http://");
  mail.concat(WiFi.localIP().toString());
  mail.concat("\n  Identifiant : ");
  mail.concat(www_username);
  mail.concat("\n  Mot de passe : ");
  mail.concat(www_password);
  send_mail("Passerelle en ligne :-)", mail);
}

void loop()
{
  // uint16_t smslen = 0;
  char smsBuffer[250];
  char fonaNotificationBuffer[64]; //for notifications from the FONA
  memset(&(fonaNotificationBuffer[0]), 0, sizeof(fonaNotificationBuffer));
  memset(&(smsBuffer[0]), 0, sizeof(smsBuffer));
  // char callerIDbuffer[32]; //we'll store the SMS sender number in here
  // memset(&(callerIDbuffer[0]), 0, sizeof(callerIDbuffer));

  // sleep(10);
  // Serial.println(F("Looop nothing"));
  // return;
  char *bufPtr = fonaNotificationBuffer; //handy buffer pointer

  // Serial.print("Etat de la connexion :");
  // Serial.println(sim800l.getNetworkStatus());

  server.handleClient();

  if (sim800l.available()) //any data available from the FONA?
  {
    Serial.println("  loop 1 : data available from FONA ...");
    int slot = 0; //this will be the slot number of the SMS
    int currslot = 0;
    int charCount = 0;
    //Read the notification into fonaInBuffer
    do
    {
      *bufPtr = sim800l.read();
      //debug
      Serial.write(*bufPtr);
      delay(1);
    } while ((*bufPtr++ != '\n') && (sim800l.available()) && (++charCount < (sizeof(fonaNotificationBuffer) - 1)));
    //Add a terminal NULL to the notification string
    *bufPtr = 0;

    // Serial.println(F("Read message #12:"));
    // sim800l.readSMS(12, smsBuffer, 250, &smslen);
    // Serial.println(smsBuffer);

    // Serial.println(F("Read message #10:"));
    // sim800l.readSMS(10, smsBuffer, 250, &smslen);
    // Serial.println(smsBuffer);

    Serial.println("  loop 2, fona buffer : ");
    Serial.println(fonaNotificationBuffer);
    Serial.println("  search in: ");
    Serial.println(FONA_PREF_SMS_STORAGE);

    //Scan the notification string for an SMS received notification.
    //  If it's an SMS message, we'll get the slot number in 'slot'
    if (1 == sscanf(fonaNotificationBuffer, "+CMTI: " FONA_PREF_SMS_STORAGE ",%d", &slot))
    {
      Serial.print(F("slot: "));
      Serial.println(slot);

      char callerIDbuffer[32]; //we'll store the SMS sender number in here

      // Retrieve SMS sender address/phone number.
      if (!sim800l.getSMSSender(slot, callerIDbuffer, 31))
      {
        Serial.println("Didn't find SMS sender in slot!");
      }
      Serial.print(F("FROM: "));
      Serial.println(callerIDbuffer);

      // Retrieve SMS value.
      uint16_t smslen;

      if (sim800l.readSMS(slot, smsBuffer, 250, &smslen))
      { // pass in buffer and max len!
        smsString = String(smsBuffer);
        Serial.println("readSMS OK...");
        Serial.println(smsString);
      }
      else
      {
        Serial.println("readSMS ERR...");
      }
      Serial.print("SMSLEN :");
      Serial.println(smslen);

      //   // if the length is zero, its a special case where the index number is higher
      //   // so increase the max we'll look at!
      //   if (smslen == 0)
      //   {
      //     Serial.print(F("[slot "));
      //     Serial.print(slot);
      //     Serial.println(F(" is empty] try next, dump buffer : "));
      //     Serial.println(smsBuffer);
      //     slot++;
      //   }
      // Serial.println(smsString);

      //Avant d'avoir le mail fonctionnel on teste avec un SMS envoyé
      //On forward au 1er
      // Serial.println("Sending reponse...");
      // if (!sim800l.sendSMS(SMS_TARGET, smsBuffer))
      // {
      //   Serial.println(F("Failed for relay 1"));
      // }
      // else
      // {
      //   Serial.println(F("Sent to relay 1!"));
      // }

      // delay(5000);

      // //Et le 2°
      // if (!sim800l.sendSMS(SMS_TARGET2, smsBuffer))
      // {
      //   Serial.println(F("Failed for relay 2"));
      // }
      // else
      // {
      //   Serial.println(F("Sent to relay 2!"));
      // }

      //Et le mail ...
      String msgSMS("Expediteur : ");
      msgSMS.concat(callerIDbuffer);
      msgSMS.concat("\n");
      msgSMS.concat("Message : ");
      msgSMS.concat(smsBuffer);
      msgSMS.concat("\n");
      Serial.println("Message Avant appel send_mail:");
      Serial.println(msgSMS);
      send_mail("Nouveau SMS", msgSMS);
      //Reset des structures de données pour eviter le bug de duplicata a chaque loop
      msgSMS.clear();
      memset(smsBuffer, 0, sizeof smsBuffer);

      // delete the original msg after it is processed
      //   otherwise, we will fill up all the slots
      //   and then we won't be able to receive SMS anymore

      // sim800l.print("AT+CMGDA=\"");
      // sim800l.println("DEL ALL\"");

      if (sim800l.deleteSMS(slot))
      {
        Serial.println(F("OK!"));
      }
      else
      {
        Serial.print(F("Couldn't delete SMS in slot "));
        Serial.println(slot);
        sim800l.print(F("AT+CMGD=?\r\n"));
        readSim800();
      }
    }
    //   slot = 0;
    // }
    Serial.println("  loop 3");
  }
  Serial.println("  loop 4");
  // sim800l.print(F("AT+CSQ\r\n"));
  // readSim800();

  sleep(2);
}
